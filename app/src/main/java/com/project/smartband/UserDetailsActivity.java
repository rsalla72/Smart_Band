package com.project.smartband;

import android.app.AlertDialog;
import android.arch.persistence.room.Room;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.project.smartband.RoomDatabase.AppDatabase;
import com.project.smartband.model.User;

public class UserDetailsActivity extends AppCompatActivity {

    private static final String TAG = UserDetailsActivity.class.getSimpleName();
    private TextView txtDetails;
    private EditText inputName, inputEmail, inputContact, inputCompany, inputUserName;
    private Button btnSave, btnBluetooth;
    private DatabaseReference mFirebaseDatabase;
    private FirebaseDatabase mFirebaseInstance;
    private FirebaseAuth mFirebaseAuth;
    private ChildEventListener mChildEventListner;
    private FirebaseUser mFirebaseUser;
    private FirebaseAuth.AuthStateListener mAuthStateListner;
    private String userId;
    private Boolean toggleStatus=false;
    AppDatabase appDatabase;
    User tempUser;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_details);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.smartband);

        init();
        appDatabase = Room.databaseBuilder(getApplicationContext(),
                AppDatabase.class, "User_Databse")
                .allowMainThreadQueries().build();
        onClickListner();
        mFirebaseInstance = FirebaseDatabase.getInstance();
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();
        userId = mFirebaseUser.getUid();

        // get reference to 'users' node
        mFirebaseDatabase = mFirebaseInstance.getReference("users");
        mFirebaseDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.child(userId).exists()){
                    toggleStatus=true;
                }else {
                    toggleStatus=false;
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        toggleButton(toggleStatus);

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String id = inputUserName.getText().toString();
                final String name = inputName.getText().toString();
                final String email = inputEmail.getText().toString();
                final String contact = inputContact.getText().toString();
                final String company = inputCompany.getText().toString();

                if(toggleStatus){
                    updateUser(id, name, email, contact, company);
                    createRoomUser();
                }else{
                    createUser(id, name, email, contact, company);
                    createRoomUser();

                }

               /* mFirebaseDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if(!(dataSnapshot.child(userId).exists())){
                            Log.e("TAG", "create user is called");
                            createUser(name, email, contact, company);
                            toggleStatus=true;
                        }else {
                            Log.e("TAG", "update user is called");

                            updateUser(name, email, contact, company);
                            toggleStatus=false;
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });*/

                // Check for already existed userId
                /*if (mFirebaseDatabase) {
                    createUser(name, email, contact, company);
                } else {
                    updateUser(name, email, contact, company);
                }*/
            }
        });


       /* mAuthStateListner = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {

                FirebaseUser fUser = firebaseAuth.getCurrentUser();
                if(fUser != null){

                    OnSignedInInitialize(fUser.getDisplayName());
                }else{
                    OnSignedOutCleanUp();
                    startActivity(new Intent(UserDetailsActivity.this, LoginActivity.class));
                }
            }
        };*/

    }

    private void createRoomUser() {
        User user = new User();
        user.setId(inputUserName.getText().toString().trim());
        user.setName(inputName.getText().toString());
        user.setEmail(inputEmail.getText().toString());
        user.setContact(inputContact.getText().toString());
        user.setCompany(inputCompany.getText().toString());
        appDatabase.userDao().insertAll(user);
        Toast.makeText(this, "User Data Inserted", Toast.LENGTH_SHORT).show();

        inputUserName.setText("");
        inputEmail.setText("");
        inputName.setText("");
        inputContact.setText("");
        inputCompany.setText("");
    }

    private void onClickListner() {
        btnBluetooth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(UserDetailsActivity.this, BluetoothActivity.class);
                startActivity(intent);
            }
        });
    }

   /* private void OnSignedOutCleanUp() {
    }

    private void OnSignedInInitialize(String userame) {
        mChildEventListner = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };
    }*/

    @Override
    protected void onResume() {
        super.onResume();
      //  mFirebaseAuth.addAuthStateListener(mAuthStateListner);
    }

    @Override
    protected void onPause() {
        super.onPause();
       // mFirebaseAuth.removeAuthStateListener(mAuthStateListner);
    }

    // Changing button text
    private void toggleButton(Boolean toggleStatus) {
        if (toggleStatus) {
            btnSave.setText(R.string.update);
        } else {
            btnSave.setText(R.string.save);
        }
    }

    /**
     * Creating new user node under 'users'
     */
    private void createUser(String id, String name, String email, String contact, String company) {
        // In real apps this userId should be fetched
        // by implementing firebase auth
        //if (TextUtils.isEmpty(userId)) {
       //     userId = inputUserName.getText().toString();
      //  }
        User user = new User(id, name, email, contact, company);

        mFirebaseDatabase.child(userId).setValue(user);

        addUserChangeListener();
    }

    private void addUserChangeListener() {
        // User data change listener
        mFirebaseDatabase.child(userId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);

                // Check for null
                if (user == null) {
                    Log.e(TAG, "User data is null!");
                    return;
                }

                Log.e(TAG, "User data is changed!" + user.name + ", " + user.email+", "+user.contact+", "+user.company);

                // Display newly updated name and email
                txtDetails.setText(user.name + ", " + user.email+", "+user.contact+", "+user.company+", "+userId);

                // clear edit text
                inputEmail.setText("");
                inputName.setText("");
                inputContact.setText("");
                inputCompany.setText("");

                toggleButton(toggleStatus);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.e(TAG, "Failed to read user", error.toException());
            }
        });
    }

    private void updateUser(String id , String name, String email, String contact, String company) {
        // updating the user via child nodes
        if (!TextUtils.isEmpty(id))
            mFirebaseDatabase.child(userId).child("id").setValue(id);

        if (!TextUtils.isEmpty(name))
            mFirebaseDatabase.child(userId).child("name").setValue(name);

        if (!TextUtils.isEmpty(email))
            mFirebaseDatabase.child(userId).child("email").setValue(email);

        if (!TextUtils.isEmpty(contact))
            mFirebaseDatabase.child(userId).child("contact").setValue(contact);

        if (!TextUtils.isEmpty(company))
            mFirebaseDatabase.child(userId).child("company").setValue(company);
    }

    private void init() {
        txtDetails =  findViewById(R.id.txt_user);
        inputName =  findViewById(R.id.name);
        inputUserName = findViewById(R.id.username);
        inputEmail =  findViewById(R.id.email);
        inputContact = findViewById(R.id.contact);
        inputCompany = findViewById(R.id.company);
        btnSave =  findViewById(R.id.btn_save);
        btnBluetooth = findViewById(R.id.bluetooth);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_item,menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // action with ID action_refresh was selected
            case R.id.search:
                final EditText editText = new EditText(this);
                editText.setHint("Enter ID");
                editText.setScaleX(0.85f);
                new AlertDialog.Builder(UserDetailsActivity.this)
                        .setTitle("Search By Id")
                        .setMessage("Enter Id")
                        .setView(editText)
                        .setPositiveButton("Search", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                tempUser = appDatabase.userDao().loadAllByIds(editText.getText().toString().trim());
                                txtDetails.setText("Name: "+tempUser.getName()+"\nContact: "+tempUser.getContact());
                            }
                        }).create().show();
               /* tempUser = appDatabase.userDao().loadAllByIds(inputUserName.getText().toString().trim());*/
                break;
        }
        return true;
    }
}
