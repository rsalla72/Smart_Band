package com.project.smartband.RoomDatabase;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.project.smartband.model.User;

@Dao
public interface UserDao {

    @Query("SELECT * FROM user WHERE id in (:userIds) LIMIT 1")
    User loadAllByIds(String userIds);

    @Insert
    void insertAll(User... users);

    @Delete
    void delete(User user);

}
