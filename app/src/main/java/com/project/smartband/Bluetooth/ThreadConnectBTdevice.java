package com.project.smartband.Bluetooth;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;

import com.project.smartband.BluetoothActivity;

import java.io.IOException;
import java.util.UUID;

public class ThreadConnectBTdevice extends Thread {
    private final BluetoothSocket mmSocket;
    private final BluetoothDevice mmDevice;
   private static final UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb");

    private ThreadConnectBTdevice(BluetoothDevice device) {
        BluetoothSocket tmp = null;
        mmDevice = device;

        try {
            tmp = device.createRfcommSocketToServiceRecord(MY_UUID);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        mmSocket=tmp;
    }

    public void run(){

    }
}
