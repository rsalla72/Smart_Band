package com.project.smartband.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

/**
 * Created by raj on 27/3/18.
 */
@Entity
public class User {
    @PrimaryKey @NonNull
    public String id;

    @ColumnInfo(name = "Name")
    public String name;

    @ColumnInfo(name = "Email")
    public String email;

    @ColumnInfo(name = "Contact")
    public String contact;

    @ColumnInfo(name = "Company")
    public String company;

    // Default constructor required for calls to
    // DataSnapshot.getValue(User.class)
    public User() {
    }

    @Ignore
    public User(String id, String name, String email, String contact, String company ) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.contact = contact;
        this.company = company;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getCompany() {
        return company;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getContact() {
        return contact;
    }
}
